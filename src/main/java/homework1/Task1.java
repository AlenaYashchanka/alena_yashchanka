package homework1;

public class Task1 {
    public static void main (String[] args) {
        int a = Integer.parseInt(args[0]);
        int p = Integer.parseInt(args[1]);

        double m1 = Double.parseDouble(args[2]);
        double m2 = Double.parseDouble(args[3]);

        final double PI = Math.PI;
        double g = 4*Math.pow(PI,2)*(Math.pow(a,3)/(Math.pow(p,2)*(m1+m2)));
        if ((Math.pow(p,2)*(m1+m2)) == 0) {
            System.out.println("Error: division by zero!");
        } else {
            System.out.println("Result G = " + g);
        }
    }
}
