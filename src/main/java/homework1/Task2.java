package homework1;

public class Task2 {
    static void loopWhile(int algorithmId, int n) {
        switch(algorithmId) {
            case 1 :
                int first = 1;
                int second = 1;
                int next;
                System.out.print(first + " " + second + " ");
                int i = 3;
                while (i <= n) {
                    next = first + second;
                    System.out.print(next + " ");
                    first = second;
                    second = next;
                    i++;
                }
                break;
            case 2 :
                int fact = 1;
                i = 1;
                while (i <= n) {
                    fact = fact * i;
                    i++;
                }
                break;
            default :
                System.out.println("Algorithmid should be equal 1 or 2!");
        }
    }

    static void loopDoWhile(int algorithmId, int n) {
        switch(algorithmId) {
            case 1 :
                int first = 1;
                int second = 1;
                int next;
                System.out.print(first + " " + second + " ");
                int i = 3;
                do {
                    next = first + second;
                    System.out.print(next + " ");
                    first = second;
                    second = next;
                    i++;
                } while (i <= n);
                break;
            case 2 :
                int fact = 1;
                i = 1;
                do {
                    fact = fact*i;
                    i++;
                } while (i <= n);
                System.out.println(fact);
            default :
                System.out.println("Algorithmid should be equal 1 or 2!");
        }
    }

    static void loopFor(int algorithmId, int n) {
        switch(algorithmId) {
            case 1 :
                int first = 1;
                int second = 1;
                int next;
                System.out.print(first + " " + second + " ");
                for (int i = 3; i <= n; i++) {
                    next = first + second;
                    System.out.print(next + " ");
                    first = second;
                    second = next;
                }
                break;
            case 2 :
                int fact = 1;
                for (int i = 1; i <= n; i++) {
                    fact = fact*i;
                }
                System.out.println(fact);
            default :
                System.out.println("Algorithmid should be equal 1 or 2!");
        }
    }

    public static void main(String[] args) {
        try {
            int algorithmId = Integer.parseInt(args[0]);
            int loopType = Integer.parseInt(args[1]);
            int n = Integer.parseInt(args[2]);
            if (n < 0) {
                throw new Exception("Enter n more or equal to 0");
            }
            if (n <= 0 && algorithmId == 1) {
                throw new Exception("Wrong number for Fibonacci");
            }
            if (algorithmId == 1 && n == 1) {
                    System.out.print("1");
                    return;
            } else if (algorithmId == 1 && n == 2) {
                    System.out.print("1 1");
                    return;
            }
            switch(loopType) {
                case 1 :
                    loopWhile(algorithmId, n);
                    break;
                case 2 :
                    loopDoWhile(algorithmId, n);
                    break;
                case 3 :
                    loopFor(algorithmId, n);
                    break;
                default :
                    System.out.println("LoopType should be 1 or 2 or 3!");
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
}
